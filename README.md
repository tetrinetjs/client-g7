# Equipe de desenvolvimento do cliente

## Objetivos:
* Desenvolver a biblioteca cliente TCP/tetrinet em nodejs 

## Grupo 7 - membros
* JOSUÉ GOMES QUEIROZ (líder)
* JHONATA LEANDRO BERNARDES PAIVA
* JONES DHYEMISON QUITO DE OLIVEIRA
* SAYMON DE OLIVEIRA SOUZA
* TONY WILLIAM MAIA DE FREITAS

## Tarefas da sprint 1 (25/03 a 31/03): 
* Criar protótipo inicial
* Criar snippets de modelos de bibliotecas nodejs