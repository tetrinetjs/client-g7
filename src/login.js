/*O usuário fornece um nick e um servidor para conectar

Caso de uso:
1 - Sucesso:
1.1 - O usuário conecta em uma partida e ganha um número de 1 a 6 que o identifica;

2 - Falha:
2.1 - O sistema informa que a partida está lotada (já tem 6 jogadores)
2.2 - O sistema informa que não conseguiu conexão com o servidor;
2.3 - O sistema informa que o nick é protegido por senha;

Obs.:
O protocolo tetrinet não é padronizado, tem versões diferentes de protocolo, falta documentação e há implementações diferentes de servidores.
Isso pode tornar o comportamento diferente conforme as decisões de implementação, por exemplo:
- o servidor tem suporte a várias salas de jogos?
- o servidor suporta nicknames protegidos por senha?
Como o desenvolvimento é incremental, nada impede de se começar com servidores mais simples e implementar as funcionalidades gradualmente.
*/
